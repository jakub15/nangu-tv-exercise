package com.example.guestbook;

import lombok.*;
import org.springframework.cloud.gcp.data.spanner.core.mapping.*;
import org.springframework.data.annotation.Id;

@Table(name="guestbook_message")
@Data
public class GuestbookMessage {
	@PrimaryKey
    @Id
    private String id;
	
	private String name;
	
	private String message;

    public GuestbookMessage() {
        this.id = java.util.UUID.randomUUID().toString();
    }
}

