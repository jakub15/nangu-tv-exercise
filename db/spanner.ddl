CREATE TABLE guestbook_message (
    id STRING(36) NOT NULL,
    name STRING(255) NOT NULL,
    message STRING(255)
) PRIMARY KEY (id);