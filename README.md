# nangu-tv-exercise

Example solution to programming trivia requested by the Nangu.tv company

Simple guestbook service ready to be deployed to the Google Cloud Platform. 

## Missing features

Properly integrate authentication and authorization
- For authentication set up Spanner as the source of the users or integrate with OAuth 2.0
- For authorization 
  - Insert into the message info about the person posting the message via custom @Query (principal)
  - Adapt the authorization mechanism to take into account the principal information. 